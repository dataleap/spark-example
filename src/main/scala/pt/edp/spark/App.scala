package pt.edp.spark


import org.apache.spark.{SparkConf, SparkContext}

import java.util.GregorianCalendar
import scala.util.Random

object App {
  def main(args: Array[String]): Unit = {
    val conf = new SparkConf().setAppName("sparkexample")
    val sc = new SparkContext(conf)

    val from = new GregorianCalendar(2017, 0, 1, 0,0,0).getTime // 2017-Jan
    val to = new GregorianCalendar(2017,1,1,0,0,0).getTime //2017-Feb
    val zeroTA = new TemporalArray(from, to, Array.fill(2976)(0D))

    val rdd = sc.makeRDD(1 to 100, 10)
    val rdd2 = rdd.map(x => {
      val randomArray = Array.fill(2976)(Random.nextDouble*100)
      new TemporalArray(from, to, randomArray)
    })

    val value = rdd2.aggregate(zeroTA)(
      (t,u) => t + u,
      (t,u) => t + u
    )

    println(value.mkString(" "))
  }
}
