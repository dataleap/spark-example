package pt.edp.spark

import java.util.Date
import scala.reflect.ClassTag

class TemporalArray[T <% Number](val min:Date, val max:Date, val array: Array[T]) extends Serializable{
  //def this(min:Date, max:Date)(implicit m: ClassTag[T]) =
  //  this(min, max, new Array[T]( (max.getTime/1000/60/15).toInt - (min.getTime/1000/60/15).toInt + 1))

  private val time = (x: Date) => (x.getTime/1000/60/15).toInt
  private val minIndex = time(min)

  def apply(x:Int) = array(x)
  def update(x:Int, v:T) = array(x) = v
  def apply(x:Date) = array(time(x)-minIndex)
  def update(x:Date, v:T) = array(time(x)-minIndex) = v
  def mkString(sep:String ) = array.mkString(sep)

  def sliceIterator(from:Date, until:Date) = {
    if(from.before(min) || until.after(max)) throw new IllegalArgumentException("Date range is outside the data range of this profile")
    array.iterator.slice(time(from)-minIndex, time(until)-minIndex)
  }

  def map[U <% Number](f:T => U)(implicit m:ClassTag[U]) = new TemporalArray[U](min, max, array.map(f))

  /*
   * Add two temporal arrays without take into consideration temporal indices
   */
  def +(that:TemporalArray[T])(implicit num: Numeric[T], m: ClassTag[T]) =
    new TemporalArray(min, max, this.array.zip(that.array).map(x => num.plus(x._1,x._2)).toArray[T])

  /*
   * Subtract two temporal arrays without take into consideration temporal indices
   */
  def -(that:TemporalArray[T])(implicit num: Numeric[T], m: ClassTag[T]) =
    new TemporalArray(min, max, this.array.zip(that.array).map(x => num.minus(x._1,x._2)).toArray[T])

  /*
   * Multiply two temporal arrays without take into consideration temporal indices
   */
  def *(that:TemporalArray[T])(implicit num: Numeric[T], m: ClassTag[T]) =
    new TemporalArray(min, max, this.array.zip(that.array).map(x => num.times(x._1,x._2)).toArray[T])
}